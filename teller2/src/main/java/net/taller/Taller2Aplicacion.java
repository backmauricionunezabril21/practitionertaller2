package net.taller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Taller2Aplicacion {

    public static void main(String[] args) {
        SpringApplication.run(Taller2Aplicacion.class, args);
    }

}
