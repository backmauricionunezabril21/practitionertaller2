package net.taller;

import net.taller.data.ProductoMongo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ProductoController {

    @Autowired
    ProductoService productoService;

    /* Get lista de productos */
    @GetMapping("/productos")
    public List<ProductoMongo> getProductos() {
        return productoService.findAll();
    }

    /*Obtener producto por Id*/
    @GetMapping("/productos/{id}" )
    public Optional<ProductoMongo> getProductoId(@PathVariable String id){
        return productoService.findById(id);
    }

    /*Añadir producto*/
    @PostMapping("/productos")
    public ProductoMongo postProductos(@RequestBody ProductoMongo newProducto){
        productoService.save(newProducto);
        return newProducto;
    }

    /*Actualizar producto*/
    @PutMapping("/productos")
    public void putProductos(@RequestBody ProductoMongo productoToUpdate){
        productoService.save(productoToUpdate);
    }

    /*Eliminar Productos*/
    @DeleteMapping("/productos")
    public boolean deleteProductos(@RequestBody ProductoMongo productoToDelete){
        return productoService.delete(productoToDelete);
    }
}

