package net.taller.data;

import org.springframework.data.mongodb.core.mapping.Document;


@Document("ProductosTaller2Mauricio")
public class ProductoMongo {

    public String id;
    public String descripcion;
    public Double precio;

    public ProductoMongo() {
    }

    public ProductoMongo(String descripcion, Double precio) {
        this.descripcion = descripcion;
        this.precio = precio;
    }

    @Override
    public String toString() {
        return String.format("Producto [id=%s, descripcion=%s, precio=%s]", id, descripcion, precio);
    }

}
